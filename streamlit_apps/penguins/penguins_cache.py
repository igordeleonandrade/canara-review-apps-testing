import matplotlib.pyplot as plt
import streamlit as st
import pandas as pd
import seaborn as sns

st.title("Penguins Dataset")
st.markdown("Create Scatterplots about penguins")

selected_species = st.selectbox("What species do you want to visualize", ['Adelie', 'Gentoo', 'Chinstrap'])

# Plot features
markers = {"Adelie": "X", "Gentoo": "s", "Chinstrap":'o'}
sns.set_style('darkgrid')

# File uploader
# This creates the UI component to upload files
dataset_file = st.file_uploader("Upload penguins dataset")


@st.cache()
def file_upload(dataset):

    if dataset is not None:
        penguins_df = pd.read_csv(dataset)

    else:
        st.stop()

    return penguins_df


penguins_df = file_upload(dataset_file)

select_x_var = st.selectbox("Select x variable for the plot",
                               ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])

select_y_var = st.selectbox("Select y variable for the plot",
                               ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])

select_gender = st.selectbox("Select penguin Gender", ['all penguins', 'male penguins', 'female penguins'])


if select_gender == "male penguins":
    penguins_df = penguins_df[penguins_df['sex'] == 'male']

elif select_gender == "female penguins":
    penguins_df = penguins_df[penguins_df['sex'] == 'female']

else:
    pass

fig, ax = plt.subplots()
ax = sns.scatterplot(data=penguins_df, x=penguins_df[select_x_var],
                     y=penguins_df[select_y_var], hue='species', markers=markers, style='species')

plt.xlabel("Selected X variable")
plt.ylabel("Selected Y variable")
plt.title(f"Scatterplot of {selected_species}")

# Streamlit takes the figure
st.pyplot(fig)
